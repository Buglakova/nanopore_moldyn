import os
import sys
from subprocess import call
import glob
f = open('size.dat','r')
system_dim = f.readlines()[0].split('\n')[0]
f.close() 


create_list = 'ls -ltr *.xyz | awk \'{print $9}\' > list.txt'
l1 = '#!/bin/bash\n'+'while IFS=\' \' read -r line || [[ -n "$line" ]];do\n'+'\techo "$line"\n'+'\tprintf "0 0 0\\n${line}\\n"> rotations\n'
l2 = '\t./gen_dist $line '+system_dim+' 1.0 5.0 rotations\n'
l3 = '\tprintf "${line}.bin ${line}.out\\n"\n'+'done < "$1"'
f = open('gendist_script.sh','w')
f.write(l1+l2+l3)
f.close()
com1 = 'bash gendist_script.sh list.txt'
call(com1,shell=True)

