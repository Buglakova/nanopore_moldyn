# Python workflow to calculate SEM current
# written by Kumar Sarthak

import os
import sys
from subprocess import call
import glob

# Add path to VMD
vmd = 'vmd'
psffile = sys.argv[1]
dcdfile = sys.argv[2]
voltage = float(sys.argv[3])

# Make xyz file from dcd and get system dimensions
getsize = vmd + ' -dispdev text -e getsize.tcl -args '+str(psffile)+' '+str(dcdfile)
xyz = vmd + ' -dispdev text -e write-xyz-dcd.tcl -args '+str(psffile)+' '+str(dcdfile)
print(getsize)
print(xyz)
call(getsize,shell=True)
call(xyz,shell=True)
f = open('size.dat','r')
system_dim = f.readlines()[0].split('\n')[0]
f.close() 

#Make binary file
create_list = "ls -ltr *.xyz | awk '{print $9}' > list.txt"
call(create_list, shell=True)
l1 = '#!/bin/bash\n'+'while IFS=\' \' read -r line || [[ -n "$line" ]];do\n'+'\techo "$line"\n'+'\tprintf "0 0 0\\n${line}\\n"> rotations\n'
l2 = '\t./gen_dist $line '+system_dim+' 1.0 5.0 rotations\n'
l3 = '\tprintf "${line}.bin ${line}.out\\n"\n'+'done < "$1"'
f = open('gendist_script.sh','w')
f.write(l1+l2+l3)
f.close()
com1 = 'bash gendist_script.sh list.txt'
call(com1,shell=True)

# Change voltage in current_fromcom
com1 = 'sed -i \'s/Volts =.*/Volts = '+ str(voltage) +'/g\' current_fromcom.py'
call(com1, shell=True)

# Run SEM 
com1 = 'python3 make-com.py'
com2 = 'python2 current_fromcom.py coms.com'
call(com1, shell=True)
call(com2, shell=True)
