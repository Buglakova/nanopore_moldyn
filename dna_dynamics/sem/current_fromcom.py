from fenics import *
import time
import numpy as np
import sys
from scipy.interpolate import RegularGridInterpolator
import time
import os

def condfrac(invec):
# points on line (min,0) (max,1)
  minr = 1.3
  maxr = 4.1
  slope = 1.0/(maxr-minr)
  int = -minr*slope
  result = slope*invec + int
  result[result<0] = 0.0000001

  result[result>1] = 1.0
  return result

def loadFunc(mesh, F, sig,interpfunction):
  vec = sig.vector()
  values = vec.get_local()

  dofmap = F.dofmap()
  my_first, my_last = dofmap.ownership_range()

  n = F.dim()
  d = mesh.geometry().dim()
  F_dof_coordinates = F.tabulate_dof_coordinates()
  F_dof_coordinates.resize((n,d))

  unowned = dofmap.local_to_global_unowned()
  dofs = filter(lambda dof: dofmap.local_to_global_index(dof) not in unowned,
              range(my_last-my_first))
  coords = F_dof_coordinates[list(dofs)]
  values[:] = interpfunction(coords)
  vec.set_local(values)
  vec.apply('insert')

def readbinGrid(name,maskRad=-1):
  if not os.path.isfile(name):
    print(name+" doesn't exist, EXITING")
    exit()
  f = open(name, 'r')
  val1d = np.fromfile(f, dtype=np.float32)

  delta = np.array([val1d[6], val1d[6], val1d[6]])
  assert np.abs(delta[0]-1)<0.00001,"WE ASSUME the DELTA IS 1!!!!, DELTA IS NOT 1"
  origin = np.array([val1d[3], val1d[4], val1d[5]])
  shape = (int(np.ceil(val1d[0])), int(np.ceil(val1d[1])), int(np.ceil(val1d[2])))
  val3d = np.reshape(val1d[7:], shape, order='F')
  if maskRad>0:
    x_ = np.arange(origin[0],origin[0]+shape[0]*delta[0],delta[0])
    y_ = np.arange(origin[1],origin[1]+shape[1]*delta[1],delta[1])
    z_ = np.arange(origin[2],origin[2]+shape[2]*delta[2],delta[2])
#  print(len(xx),val3d.shape[0])
    assert len(x_) == val3d.shape[0], "x is wrong size"
    assert len(y_) == val3d.shape[1], "y is wrong size"
    assert len(z_) == val3d.shape[2], "z is wrong size"
    xx,yy,zz = np.meshgrid(x_,y_,z_, indexing='ij')

    msk = xx*xx+yy*yy>maskRad*maskRad
    val3d[msk] = 0.00001
  L = delta[0]*shape[0]; W = delta[1]*shape[1];H = delta[2]*shape[2]
  nx = int(shape[0])
  ny = int(shape[1])
  nz = int(shape[2])
  Lm = L-delta[0]
  Wm = W-delta[1]
  Hm = H-delta[2]

  #####CRUFT
#  bulkR = 1./sigma
#  fullOpenR = bulkR*H/(L*W)
#  fullOpenG = 1./fullOpenR

  return val3d , [Lm,Wm,Hm], [nx,ny,nz]

start_time = time.time()
prev_time = start_time

#mpiRank = MPI.rank(mpi_comm_world())
#### MPIRANK ISN"T WORKING
mpiRank = 0
size = 120.0
sizex = size
sizey=size
sizez=size
numpts = 121
numx = numpts
numy=numpts
numz=numpts
Volts = -0.22
####################read in grid#############
#binfileName = sys.argv[1]
#outfileName = sys.argv[2]
comfile = sys.argv[1]
infiles = []
outfiles = []
with open(comfile, 'r') as infile:
  count = 0
  for line in infile:
    count+=1
    if count %2 ==0 and line.strip() != '':
      infiles.append(line.split()[0]+".bin")
      outfiles.append(line.split()[0]+".dat")

####################finish read in grid############

sigma=1.660843  #1M
solver = KrylovSolver("gmres", "amg")
#solver = KrylovSolver("gmres", "none")
#solver = LinearSolver("mumps")
#parameters['krylov_solver']['nonzero_initial_guess'] = True
#parameters["krylov_solver"]["monitor_convergence"] = True
#solver.parameters["linear_solver"] = "mumps"
#PETScOptions.set('ksp_rtol', '.05')
solver.parameters["relative_tolerance"] = 1e-8
solver.parameters["maximum_iterations"] = 20000
#solver.parameters["monitor_convergence"] = True
set_log_level(30)

val3d,[Lm,Wm,Hm],[nx,ny,nz] = readbinGrid(infiles[0])
sizex = Lm-5
#sizex = Lm
sizey = Wm-5
#sizey = Wm
sizez = Hm
numx = int(sizex)
numy = int(sizey)
numz = int(sizez)

if 1:
  calcSig = sigma*condfrac(val3d)
  interpfunction = RegularGridInterpolator((np.linspace(-Lm/2.,Lm/2.,num=nx),np.linspace(-Wm/2.,Wm/2.,num=ny),np.linspace(-Hm/2.,Hm/2.,num=nz)),calcSig,bounds_error=False,fill_value=sigma)



# Create mesh and define function space
mesh = BoxMesh(Point(-sizex/2., -sizey/2., -sizez/2.), Point(sizex/2., sizey/2. , sizez/2.), numx, numy,numz)
V = FunctionSpace(mesh, 'P', 1)
F = FunctionSpace(mesh, 'CG', 1)

###########



tol = 1E-14

class TopBoundary(SubDomain):
  def inside(self,x,on_boundary):
    return on_boundary and near(x[2],sizez/2.) 

class BotBoundary(SubDomain):
  def inside(self,x,on_boundary):
    return on_boundary and near(x[2],-sizez/2.) 

top_bndr = TopBoundary()
bot_bndr = BotBoundary()

VTop = DirichletBC(V, Constant(Volts), top_bndr)
VBot = DirichletBC(V, Constant(0), bot_bndr)

#boundary_parts = FacetFunction("size_t", mesh)
boundary_parts = MeshFunction("size_t", mesh,mesh.topology().dim() - 1)
top_bndr.mark(boundary_parts,1)
bot_bndr.mark(boundary_parts,2)


# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
#f = Constant(1)
sig = Function(F)
u1 = Function(V)
l = Constant(0.0)*v*dx 

for I in range(len(infiles)):
  val3d,[Lm,Wm,Hm],[nx,ny,nz] = readbinGrid(infiles[I])
  calcSig = sigma*condfrac(val3d)
  interpfunction = RegularGridInterpolator((np.linspace(-Lm/2.,Lm/2.,num=nx),np.linspace(-Wm/2.,Wm/2.,num=ny),np.linspace(-Hm/2.,Hm/2.,num=nz)),calcSig,bounds_error=False,fill_value=sigma)
  loadFunc(mesh,F,sig,interpfunction)
  FF = sig*dot(grad(u), grad(v))*dx
  a, L = lhs(FF), rhs(FF)

  A,bb = assemble_system(a,l,[VTop,VBot])
#  b = assemble(L)
  solver.set_operator(A)

  # Compute solution
#  solve(a == L, u1, [VTop,VBot])
#  solve(a == L, u1, [VTop,VBot])
  solver.solve( u1.vector(),bb)
  if mpiRank ==0:
    cur_time = time.time()
    print("Total time: {:.2f}, prev calc {:.2f}, {} of {}".format(cur_time-start_time,cur_time-prev_time,I+1,len(infiles)))
    if I==0:
      first_time=cur_time
    prev_time = cur_time
  #dof_coordinates = V.tabulate_dof_coordinates()



  ds = ds(subdomain_data = boundary_parts)

  flux_top = dot(Constant((0,0,1)),sig*nabla_grad(u1))*ds(1)
  flux_bot = dot(Constant((0,0,1)),sig*nabla_grad(u1))*ds(2)
  ft = assemble(flux_top)
  fb = assemble(flux_bot)

  if mpiRank ==0:
    openG = sigma/(sizez/(sizex*sizey))
    fullBoxG = ft/Volts
#    print("deltaG = {}, openG = {:.4f}, blockedG = {:.4f}".format(openG-fullBoxG,openG,fullBoxG))
    outp = np.array([ft])
    np.savetxt(outfiles[I],outp)
tottime = time.time()-start_time
excfirst = time.time()-first_time
if len(infiles)>1:
  print("Total Seconds: {:.3f}, Seconds/Calc (excluding first): {:.3f}".format(tottime,excfirst/(len(infiles)-1.)))
else:
  print("Total Seconds: {:.3f}".format(tottime,))
#    print("Current through Top:    {:.3f}".format(ft))
#    print("Current through Bottom: {:.3f}".format(ft))
    #vtkfile << (u)    5    ALA  CB   CT3   -0.2
