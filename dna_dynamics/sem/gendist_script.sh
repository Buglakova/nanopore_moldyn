#!/bin/bash
while IFS=' ' read -r line || [[ -n "$line" ]];do
	echo "$line"
	printf "0 0 0\n${line}\n"> rotations
	./gen_dist $line 105.0 105.0 222.0 0.0 0.0 0.0 1.0 5.0 rotations
	printf "${line}.bin ${line}.out\n"
done < "$1"