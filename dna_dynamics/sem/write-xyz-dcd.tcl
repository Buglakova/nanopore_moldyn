set in1 [lindex $argv 0]
set in2 [lindex $argv 1]

# out dx file
if {1} {
   set startFrame 0
   set stride 10 
   set df 1
   set dir .
   mol delete all
   mol new $in1
   animate delete all
   mol addfile $in2 type xtc step $stride waitfor all

   set nFrames [molinfo top get numframes]
   puts [format "Reading %i frames." $nFrames]   
   
   for {set f 0} {$f < $nFrames} {incr f $df}  {
       molinfo top set frame $f
       set m [expr $f+$startFrame]   
       set sel [atomselect top "(not water) and (not ions)"]
       set num [$sel num]
       set coord [$sel get {x y z}]
       set radius [$sel get radius]
       set output [open test-$m.xyz w] 
       foreach xyz $coord r $radius {
              puts $output "$xyz $r"
       }
       $sel delete
       close $output
       puts "$m $num"
   }
}
exit
