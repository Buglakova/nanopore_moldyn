set in1 [lindex $argv 0]
set in2 [lindex $argv 1]
set output [open size.dat w]
# out dx file
if {1} {
    mol load pdb $in1
    mol addfile $in2 type xtc first 0 last 0
    set sel [atomselect top water]
    set a [measure minmax $sel]
    set a1 [expr floor([lindex [lindex $a 1] 0])]
    set a2 [expr floor([lindex [lindex $a 1] 1])]
    set a3 [expr floor([lindex [lindex $a 1] 2])]
    set a4 [expr floor([lindex [lindex $a 0] 0])]
    set a5 [expr floor([lindex [lindex $a 0] 1])]
    set a6 [expr floor([lindex [lindex $a 0] 2])]
    puts $output "$a1 $a2 $a3 $a4 $a5 $a6"
}
close $output
exit
