# Data structure to open density maps and make analysis
import numpy as np 
import os
import matplotlib.pyplot as plt
from scipy.ndimage import rotate
from itertools import chain

class density_map():
    def __init__(self):
        self.grid = None
        self.origin = None
        self.delta = None
        
        self.map = {}
        
        self.X = None
        self.Y = None
        self.Z = None

    def maps_from_npz(self, npz_file):
        self.map = dict(npz_file)
        for key in self.map.keys():
            self.map[key][self.map[key] < 1e-10] = 0
        self.grid = self.map["wat"].shape
        self.origin = None
        self.delta = [1, 1, 1]
        self.X = np.arange(0, self.delta[0] * self.grid[0], self.delta[0])
        self.Y = np.arange(0, self.delta[1] * self.grid[1], self.delta[1])
        self.Z = np.arange(0, self.delta[2] * self.grid[2], self.delta[2])


    def circular_average(self, symm_k=1):
        for key in self.map.keys():
            if key:
                emap = self.map[key]
                rotated = np.array([rotate(emap,  i * 360 / symm_k, reshape=False, mode='nearest') for i in range(symm_k)])
                self.map[key] = np.mean(rotated, axis=0)
                self.map[key][self.map[key] < 1e-10] = 0


    def read_map(self, file_name, map_type):
        # Open map file
        dx_file = open(file_name, 'r')
        print(file_name)
        dx_file = dx_file.readlines()

        # Read information about the grid
        self.grid = list(map(int, dx_file[1].split()[-3:]))
        self.origin = list(map(float, dx_file[2].split()[-3:]))
        self.delta = list([list(map(float, dx_file[i].split()[-3:]))[i - 3] for i in range(3, 6)])

        # Read data and convert to 3D array
        den_map = dx_file[8:-2]
        den_map = list(map(lambda x: list(map(float, x.split())), den_map))
        den_map_flatten = list(chain.from_iterable(den_map))
        den_map = np.array(den_map_flatten)
        den_map = den_map.reshape(self.grid)
        
        # Sum every 3 angstroms
        shape = den_map.shape
        # print(shape)
        shape = list(shape)
        shape_short = int(shape[2] / 3)
        print(shape)
        self.map[map_type] = np.zeros(shape)
        for i in range(shape_short):
            self.map[map_type][:, :, i * 3] = den_map[:, :, i * 3 : (i + 1) * 3].sum(axis=2) / 3
            if shape[2] > i * 3 + 1:
                self.map[map_type][:, :, i * 3 + 1] = self.map[map_type][:, :, i * 3] / 3
            if shape[2] > i * 3 + 2:
                self.map[map_type][:, :, i * 3 + 2] = self.map[map_type][:, :, i * 3] / 3
            
        # self.delta[2] = self.delta[2] * 3
        # self.grid[2] = shape[2]
        
        # Not sum every 3 angstroms
        # self.map[map_type] = den_map

        # For plots
        self.X = np.arange(0, self.delta[0] * self.grid[0], self.delta[0])
        self.Y = np.arange(0, self.delta[1] * self.grid[1], self.delta[1])
        self.Z = np.arange(0, self.delta[2] * self.grid[2], self.delta[2])
        print("Read density map " + map_type + ". Size:", *self.map[map_type].shape)
        
    def plot(self, map_type, x = None, y = None, z = None): 
        if x is not None and y is not None:
            map_slice = self.map[map_type][x, y, :].T
            print(map_slice.shape)
            plt.plot(self.Z, map_slice)
            plt.xlabel("z, nm")
            return self.Z, map_slice
            
        elif x is not None and z is not None:
            map_slice = self.map[map_type][x, :, z].T
            print(map_slice.shape)
            plt.plot(self.Y, map_slice)
            plt.xlabel("y, nm")
            return self.Y, map_slice
        
        elif y is not None and z is not None:
            map_slice = self.map[map_type][:, y, z].T
            print(map_slice.shape)
            plt.plot(self.X, map_slice)
            plt.xlabel("x, nm")
            return self.X, map_slice
        
        elif x is not None:
            X = self.map[map_type][x, :, :].T
            print(X.shape)
            plt.contourf(self.Y, self.Z, X, 20, cmap='RdYlBu')
            plt.colorbar()
            plt.xlabel('y, nm')
            plt.ylabel('z, nm')
            
        elif y is not None:
            Y = self.map[map_type][:, y, :].T
            print(Y.shape)
            plt.contourf(self.X, self.Z, X, 20, cmap='RdYlBu')
            plt.colorbar()
            plt.xlabel('x, nm')
            plt.ylabel('z, nm')
            
        elif z is not None:
            Z = self.map[map_type][:, :, z].T
            print(Z.shape)
            plt.contourf(self.X, self.Y, Z, 20, cmap='RdYlBu')
            plt.colorbar()
            plt.xlabel('x, nm')
            plt.ylabel('y, nm')
    
    def find_center(self):
        
        # Find center of mass of the protein and take x and y coordinates of it
        grid = np.meshgrid(self.X, self.Y, indexing='ij')
        prot_sum = self.map['prot'].sum(axis = 2)
        self.x_av = np.average(grid[0], weights=prot_sum)
        self.y_av = np.average(grid[1], weights=prot_sum)

    
    def find_r(self, lim_ratio = 0.2):
        self.find_center()
        self.r = []
        self.wat_density = []
        self.cla_density = []
        self.pot_density = []
        # print('X', self.X[-1])
        # print(self.map.keys())
        i = 0
        for array, prot in zip(np.rollaxis(self.map['wat'], 2), np.rollaxis(self.map['prot'], 2)):
            r = 2
            dr = 1
            ratio = 1
            while ratio >= lim_ratio and r < self.Y[-1] / 2:
                mask = np.logical_and(((self.X[np.newaxis, :] - self.x_av) ** 2 + (self.Y[:, np.newaxis] - self.y_av) ** 2 < (r + dr) ** 2),
                                      ((self.X[np.newaxis, :] - self.x_av) ** 2 + (self.Y[:, np.newaxis] - self.y_av) ** 2 > r ** 2))
                r += dr
                if np.average(prot[mask.T]) > 0:
                    ratio = np.average(array[mask.T]) / np.average(prot[mask.T])

            self.r.append(r)
        return self.r, self.x_av
    
    def make_density_profiles(self):
        self.profile = {map_type: self.density_profile(map_type) for map_type in self.map.keys()}
        return self.profile
        
    def density_profile(self, map_type):
        return [self.average_dens(array, r) for (array, r) in zip(np.rollaxis(self.map[map_type], 2), self.r)]
        
    def average_dens(self, array, r):
        mask = ((self.X[np.newaxis,:] - self.x_av) ** 2 + (self.Y[:,np.newaxis] - self.y_av) ** 2 < r ** 2)
        return np.sum(array[mask.T]) / (np.pi * r ** 2 * self.delta[2])
    
    def delete_maps(self):
        del(self.map)
    
    def __str__(self):
        return '0'
    
    def __repr__(self):
        return '0'