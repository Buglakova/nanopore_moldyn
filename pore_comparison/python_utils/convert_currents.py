import numpy as np 
import os
import pickle

def read_current(file):
    flines = file.readlines()
    time = [float(line.split()[0]) for line in flines]
    current = [float(line.split()[1]) for line in flines]
    return time, current

def convert_currents(path):
    file_list = os.listdir(path)
    file_list = [f for f in file_list if f[-4:] == ".dat"]
    print(file_list)
    names = set([f.split("_")[1] for f in file_list])
    currents = {}
    for name in names:
        curr_name = {}
        for curr_type in ["curr", "currPOT", "currCLA"]:
            curr_name[curr_type] = [0]
            curr_name["time"] = [0]
            for part in ["part1", "part2"]:
                file_name = curr_type + "_" + name + "_" + part + ".dat"
                file_name = os.path.join(path, file_name)
                if os.path.isfile(file_name):
                    cur_file = open(file_name, "r")
                    time, c = read_current(cur_file)
                    cur_file.close()
                    curr_name[curr_type] += c[1:]
                    time = [t + curr_name["time"][-1] for t in time[1:]]
                    curr_name["time"] += time
            if name == "csgg":
                curr_sign = -1
            else:
                curr_sign = 1
            curr_name[curr_type] = curr_sign * np.array(curr_name[curr_type])
            curr_name["time"] = np.array(curr_name["time"])
            currents[name] = curr_name
    with open(os.path.join(path, "processed_currents.pickle"), "wb") as handle:
        pickle.dump(currents, handle)




if __name__ == "__main__":
    convert_currents("/home/racoon/different_pores/currents")