# %%
import matplotlib.pyplot as plt
import numpy as np
from itertools import chain
import os
import sys
from scipy.ndimage import rotate

def read_map(file_name, voltage=0.15, ewald_factor = 0.0258):
    dx_file = open(file_name, 'r')
    print(file_name)
    dx_file = dx_file.readlines()

    # Read information about the grid
    grid = list(map(int, dx_file[1].split()[-3:]))
    origin = list(map(float, dx_file[2].split()[-3:]))
    delta = list([list(map(float, dx_file[i].split()[-3:]))[i - 3] for i in range(3, 6)])

    # Read data and convert to 3D array
    den_map = dx_file[8:-5]
    den_map = list(map(lambda x: list(map(float, x.split())), den_map))
    den_map_flatten = list(chain.from_iterable(den_map))
    den_map = np.array(den_map_flatten)
    den_map = den_map.reshape(grid)
    den_map = den_map * ewald_factor
    linear_voltage = np.linspace(voltage, 0, den_map.shape[2])
    linear_voltage = np.stack([linear_voltage] * den_map.shape[1])
    linear_voltage = np.stack([linear_voltage] * den_map.shape[0])
    print(linear_voltage.shape)
    den_map = den_map + linear_voltage
    print("Read density map. Size:", den_map.shape)
    return den_map


def circular_average(emap, symm_k=1):
    rotated = np.array([rotate(emap,  i * 360 / symm_k, reshape=False, mode='nearest') for i in range(symm_k)])
    return np.mean(rotated, axis=0)


def find_center(emap):
    grid = np.meshgrid(range(emap.shape[0]), range(emap.shape[1]), indexing='ij')
    emap_sum = emap.sum(axis = 2)
    x_av = np.average(grid[0], weights=emap_sum)
    y_av = np.average(grid[1], weights=emap_sum)
    return x_av, y_av


def average_dens(array, r, x_av, y_av):
    z_step = 1
    X = np.arange(array.shape[0])
    Y = np.arange(array.shape[1])
    mask = ((X[np.newaxis,:] - x_av) ** 2 + (Y[:,np.newaxis] - y_av) ** 2 <= r ** 2)
    return np.sum(array[mask.T]) / (np.pi * r ** 2 * z_step)


def find_emap_profile(emap, lim_diff=0.025):
    x_av, y_av = find_center(emap)
    r_all = []
    X = np.arange(emap.shape[0])
    Y = np.arange(emap.shape[1])
    if lim_diff == 100:
        aerolysin = True
        lim_diff = 0.025
    else:
        aerolysin = False
    i = 0
    for array in np.rollaxis(emap, 2):
        r = 2
        dr = 1
        diff = 0
        if aerolysin and (i < 25 or i > 140):
            r_all.append(emap.shape[0] / 2)
        else:
            while diff <= lim_diff and r < emap.shape[0] / 2:
                mask_ring = np.logical_and(((X[np.newaxis, :] - int(x_av)) ** 2 + (Y[:, np.newaxis] - int(y_av)) ** 2 <= (r + dr) ** 2),
                                        ((X[np.newaxis, :] - int(x_av)) ** 2 + (Y[:, np.newaxis] - int(y_av)) ** 2 >= r ** 2))
                mask_disk = ((X[np.newaxis, :] - x_av) ** 2 + (Y[:, np.newaxis] - y_av) ** 2 <= r ** 2)
                r += dr
                diff = np.average(array[mask_ring.T]) - np.average(array[mask_disk.T])
            r_all.append(r)
        i += 1

    emap_profile = [average_dens(array, r, x_av, y_av) for (array, r) in zip(np.rollaxis(emap, 2), r_all)]
    return x_av, y_av, r_all, emap_profile


 
