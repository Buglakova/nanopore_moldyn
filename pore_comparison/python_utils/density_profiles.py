import numpy as np
import os
import pickle
from density_utils import density_map

def get_profile(pore_path, symmetry, proportion):
    density_path = os.path.join(pore_path, "density")

    density_objects = []
    profile = {}
    for map_name in os.listdir(density_path):
        print(map_name)
        map_path = os.path.join(density_path, map_name)
        den_map = density_map()
        den_map.maps_from_npz(np.load(map_path))
        den_map.circular_average(symmetry)

        # Sum every 3 angstrom
        for mt in den_map.map.keys():
            shape = den_map.map[mt].shape
            # print(shape)
            cur_map = den_map.map[mt]
            shape = list(shape)
            shape_short = int(shape[2] / 3)
            # print(shape)
            den_map.map[mt] = np.zeros(shape)
            for i in range(shape_short):
                den_map.map[mt][:, :, i * 3] = cur_map[:, :, i * 3 : (i + 1) * 3].sum(axis=2)
                if shape[2] > i * 3  + 1:
                    den_map.map[mt][:, :, i * 3 + 1] = den_map.map[mt][:, :, i * 3 ]
                if shape[2] > i * 3  + 2:
                    den_map.map[mt][:, :, i * 3 + 2] = den_map.map[mt][:, :, i * 3 ]

        
        r, x_av = den_map.find_r(proportion)
        den_map.make_density_profiles()
        density_objects.append(den_map)

    r = np.array([den_map.r for den_map in density_objects])
    r_mean = np.median(r, axis=0)
    r_var = np.var(r, axis=0)
    profile["r"] = r_mean
    profile["r_var"] = r_var

    x_av = np.array([den_map.x_av for den_map in density_objects])
    x_av = np.mean(x_av, axis=0)
    profile["x_av"] = x_av
    y_av = np.array([den_map.y_av for den_map in density_objects])
    y_av = np.mean(y_av, axis=0)
    profile["y_av"] = y_av

    for map_type in ["wat", "pot", "cla"]: 
        d = [den_map.density_profile(map_type) for den_map in density_objects]
        d = np.array(d)
        d_mean = np.mean(d, axis=0)
        d_var = np.var(d, axis=0)
        profile[map_type] = d_mean
        profile[map_type + "_var"] = d_var

    mean_densities = {}
    for t in density_objects[0].map.keys():
        mean_densities[t] = np.array([d.map[t] for d in density_objects])
        mean_densities[t] = np.mean(mean_densities[t], axis=0)

    return mean_densities, profile

if __name__ == "__main__":
    pores = ["wt", "k238a", "ahem", "mspa", "csgg"]
    path = "/home/racoon/different_pores"
    proportions = {"ahem": 0.2, "mspa": 0.2, "k238a": 0.4, "wt": 0.4, "csgg": 0.4}
    symmetry = {"ahem": 7, "mspa": 8, "k238a": 7, "wt": 7, "csgg": 9}
    pore_paths = {pore: os.path.join(path, pore) for pore in pores}

    mean_dens_name = "mean_densities.npz"
    profile_name = "density_profile.pickle"
    
    for pore in pores:
        print(pore)
        
        mean_path = os.path.join(pore_paths[pore], mean_dens_name)
        profile_path = os.path.join(pore_paths[pore], profile_name)
        mean_dens, profile = get_profile(pore_paths[pore], symmetry[pore], proportions[pore])

        np.savez(mean_path, **mean_dens)
        with open(profile_path, "wb") as handle:
            pickle.dump(profile, handle)