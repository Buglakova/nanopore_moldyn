import numpy as np
from itertools import chain
import os
import sys

def read_map(file_name):
    dx_file = open(file_name, 'r')
    print(file_name)
    dx_file = dx_file.readlines()

    # Read information about the grid
    grid = list(map(int, dx_file[1].split()[-3:]))
    origin = list(map(float, dx_file[2].split()[-3:]))
    delta = list([list(map(float, dx_file[i].split()[-3:]))[i - 3] for i in range(3, 6)])

    # Read data and convert to 3D array
    den_map = dx_file[8:-2]
    den_map = list(map(lambda x: list(map(float, x.split())), den_map))
    den_map_flatten = list(chain.from_iterable(den_map))
    den_map = np.array(den_map_flatten)
    den_map = den_map.reshape(grid)
    print("Read density map. Size:", den_map.shape)
    return(den_map)

if __name__ == "__main__":
    N = int(sys.argv[2])
    number = sys.argv[1]
    print("number", number)
    print("N", N)
    map_names = ['wat', 'nonwat', 'prot', 'cla', 'pot']
    # map_names = ['cla', 'pot']
    maps = {name: [] for name in map_names}
    for i in range(0, N + 1):
        for m in map_names:
            filename = os.path.join("density/", m + '_dens_frame_' + str(i) + '.dx')
            den_map = read_map(filename)
            os.remove(filename)
            maps[m].append(den_map)
    maps = {name: np.mean(np.stack(maps[name]), axis=0) for name in map_names}

    # print(maps["wat"].shape)
    np.savez("density/maps_" + str(number) + ".npz", **maps)
    # plt.imshow(den_map[60, :, :])
    # plt.savefig("try.png")



