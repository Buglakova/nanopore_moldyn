set outfile "water_com.txt"

set N [molinfo top get numframes]
puts [format "Processing %i frames." $N]
set water [atomselect top "protein"]

set out [open "$outfile" w]

for {set i 1} {$i <= $N} {incr i} {

    puts $i
    molinfo top set frame $i

    set com [measure center $water]

    puts $out "$i $com"


}