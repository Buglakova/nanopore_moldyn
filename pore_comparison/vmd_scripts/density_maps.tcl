proc min args {
    set res [lindex $args 0]
    foreach element [lrange $args 1 end] {
        if {$element < $res} {set res $element}
    }
    return $res
}

set head_wat "density/wat_dens_frame_"
set head_prot "density/prot_dens_frame_"
set head_non_wat "density/nonwat_dens_frame_"
set head_chloride "density/cla_dens_frame_"
set head_potassium "density/pot_dens_frame_"

set N [molinfo top get numframes]
# set N 3
puts [format "Processing %i frames." $N]
set all [atomselect top "all"]
set box [measure minmax $all]
set stride 10

set all [atomselect top "all"]
set wat [atomselect top "resname TIP3"]
set protein [atomselect top "protein"]
set non_wat [atomselect top "not resname TIP3 and not ions"]
set chloride [atomselect top "resname CLA"]
set potassium [atomselect top "resname POT"]

for {set j 1} {[expr $j + $stride] <= $N} {incr j [expr $stride + 1]} {

	for {set i 0} {$i <= $stride} {incr i} {
		molinfo top set frame [expr $j + $i]
		# Water	
		$wat frame $i	
		set  filename $head_wat
		append filename "$i.dx"
		volmap density $wat -res 1 -o $filename -minmax $box

		# Protein
		$protein frame $i  
		set  filename $head_prot
		append filename "$i.dx"
		volmap density $protein -res 1 -o $filename -minmax $box

		# Non water
		$non_wat frame $i  
		set  filename $head_non_wat
		append filename "$i.dx"
		volmap density $non_wat -res 1 -o $filename -minmax $box

		# Chloride
		$chloride frame $i  
		set  filename $head_chloride
		append filename "$i.dx"
		volmap density $chloride -res 1 -o $filename -minmax $box

		# Potassium
		$potassium frame $i  
		set  filename $head_potassium
		append filename "$i.dx"
		volmap density $potassium -res 1 -o $filename -minmax $box

	
	}

	exec python average_maps.py $j $stride
}


